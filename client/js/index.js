define([ "/twitter-bootstrap/2.3.0/js/bootstrap-tab.js" ], function () {
  'use strict';

  $(document).off('click.tab.data-api', '[data-toggle="tab"], [data-toggle="pill"]');

  /**
   * @name TabView
   * @class Tab view implementation based on <a href="http://twitter.github.com/bootstrap/javascript.html#tabs">
   *     Twitter Bootstrap</a> tabs.
   *
   * <p>Compared to Bootstrap, the markup is enriched with semantic tags and annotations to fulfill
   * <a href="http://dev.opera.com/articles/view/introduction-to-wai-aria/">ARIA</a> criteria
   * (Details on implementing an ARIA tab view:
   * <a href="http://test.cita.uiuc.edu/aria/tabpanel/tabpanel1.php">[1]</a>,
   * <a href="http://www.accessibleculture.org/articles/2010/08/aria-tabs/">[2]</a>).
   * Therefore, you can also navigate the control via keyboard:</p>
   *
   * <table style="text-align: left">
   *   <tr><th>Key</th><th>Shows ...</th></tr>
   *   <tr><td><tt>Pos1</tt></td><td>first tab</td></tr>
   *   <tr><td><tt>End</tt></td><td>last tab</td></tr>
   *   <tr><td><tt>left</tt></td><td>previous tab</td></tr>
   *   <tr><td><tt>right</tt></td><td>next tab</td></tr>
   * </table>
   */
  function TabView() {}

  TabView.prototype =
  /** @lends TabView */
  {

    start: function () {
      var $root = this.context.getRoot();

      this.$tabList = $root.find('.nav-tabs');
      this.$tabs = $root.find('.tab-content');

      this.$tabList.on('click', 'a', this._onClick.bind(this))
                   .on('keypress', 'a:active', this._onKeyPress.bind(this));
    },

    _onClick: function(e) {
      e.preventDefault();
      this.show(e.target);
    },

    _onKeyPress: function(e) {
      var key = e.keyCode;

      if ([ 35, 36, 37, 39 ].indexOf(key) !== -1) {
        e.preventDefault();
      }

      switch(key) {
      case 35: this.show('a:last');  break; // End
      case 36: this.show('a:first'); break; // Pos1
      case 37: this.show('prev');    break; // Left
      case 39: this.show('next');    break; // Right
      }
    },

    /**
     * Adds a new tab and the corresponding tab pane.
     *
     * <p>A tab is basically a link (<code>&lt;a&gt;</code>). Use the reference to a link to bring its tab pane to front &rArr; <code>show()</code>.</p>
     *
     * @param {Map} options These keys are currently supported:
     * <dl>
     *   <dt><code>label</code></dt>
     *   <dd>The tab label</dd>
     *   <dt><code>id</code></dt>
     *   <dd>The id of the tab pane element, which is used for navigation</dd>
     *   <dt><code>cssClass</code></dt>
     *   <dd>An CSS class for the tab (list item)</dd>
     * </dl>
     * @param {String} [content="<div>"] The selector for the content element of this tab pane
     */
    add: function(options, content) {
      this._addTab(options);
      this._addTabPane(content, options.id);
      this._update();

      // highlight the element, which has been added first
      var isFirst = (this.$tabList.children().length === 1);

      if (isFirst) {
        this.show('a:first');
      }
    },

    _addTab: function(options) {
      var $tab = $('<li>').attr('role', 'tab');
      var $a = $('<a>')
          .attr('href', '#' + options.id) // bootstrap auto-wires a[href] to div[id]
          .text(options.label);

      if (options.cssClass) {
        $tab.addClass(options.cssClass);
      }

      $tab.append($a);

      this.$tabList.append($tab);
    },

    _addTabPane: function(content, id) {
      var $tabPane = $(content || '<div>') // default content: an empty <div>
          .attr({ id: id, role: 'tabpanel' })
          .addClass('tab-pane');

      this.$tabs.append($tabPane);
    },

    /**
     * Shows the tab pane, which corresponds to the link (<code>&lt;a&gt;</code>), which is addressed by this selector.
     *
     * <p>Besides regular CSS selectors like <code>"a:first"</code>, also "prev" and "next" are supported.
     * These show the tab before or after the currently shown one.</p>
     *
     * @param {String} selector A CSS selector or <code>"prev"/"next"</code>
     */
    show: function(selector) {
      var $tabList = this.$tabList, $active = $tabList.find('.active'), $tab;

      switch (selector) {
      // prev and next cannot be addressed by a regular selector
      case 'prev': $tab = $active.prev().find('a'); break;
      case 'next': $tab = $active.next().find('a'); break;
      default:     $tab = $tabList.find(selector);
      }

      $tab.tab('show');
      this._update();
    },

    _update: function() {
      var $tabList = this.$tabList;

      $tabList.find('.active a').focus();
      $tabList.find('a').attr('tabindex', function() {
        return $(this).parent().hasClass('active') ? 0 : -1; // remove inactive tabs from tab order
      });

      this.$tabs.children().attr('aria-hidden', function() {
        return ! $(this).hasClass('active');
      });
    }

  };

  return TabView;

});

